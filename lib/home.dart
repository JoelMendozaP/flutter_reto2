import 'package:flutter/material.dart';

//String pathImage = "assets/img/1.jpg";
class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Container(
      color: Colors.orange,
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          tarjeta2('BRASIL','PASAJE: 300Bs.','assets/img/1.jpeg'),
          tarjeta1('CHINA','PASAJE: 1000Bs.','assets/img/2.jpeg'),
          tarjeta2('KOREA','PASAJE: 800Bs.','assets/img/3.jpeg'),
          tarjeta1('EE.UU.','PASAJE: 950Bs.','assets/img/4.jpeg'),
          tarjeta2('PARIS','PASAJE: 1500Bs.','assets/img/5.jpeg'),
        ],
      ),
    );
  }

  Widget tarjeta1(String lugar, String price,String img){
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7,horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          //border: Border.all(width: 15, color: Colors.white),
          borderRadius: BorderRadius.circular(5)
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          //Imagen
          inf(lugar, price),
          card(img),
          //imagen(),
        ],
      ),
    );
  }
  Widget tarjeta2(String lugar, String price,String img){
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7,horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          //border: Border.all(width: 15, color: Colors.white),
          borderRadius: BorderRadius.circular(5)
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          //Imagen
          card(img),
          inf(lugar, price),
        ],
      ),
    );
  }

  Widget inf(String lugar, String price){
    return Container(
      height: 150,
      margin: EdgeInsets.symmetric(vertical: 7,horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          pais(lugar),
          precio(price),
          estrellas()
        ],
      ),
    );
  }
  Widget pais(String lugar){
    return Container(
      child: Text(lugar,style: TextStyle(color: Colors.black, fontSize: 30)),
    );
  }
  Widget precio(String price){
    return Container(
      child: Text(price,style: TextStyle(color: Colors.black,fontSize: 15)),
    );
  }
  Widget estrellas(){
    return Container(
      child: Row(
        children: <Widget>[
          Icon(Icons.star, color: Colors.orange, size: 25),
          Icon(Icons.star, color: Colors.orange, size: 25),
          Icon(Icons.star, color: Colors.orange, size: 25),
          Icon(Icons.star, color: Colors.orange, size: 25),
          Icon(Icons.star_half, color: Colors.orange, size: 25),
        ],
      ),
    );
  }
  Widget card(String img){
    return Container(
      height: 170.0,
      width: 170,
      
      decoration: BoxDecoration(
        image: DecorationImage(
          
          fit: BoxFit.cover,
          image: AssetImage(img)
          ),
          borderRadius: BorderRadius.all(Radius.circular(0),),
          shape: BoxShape.rectangle,
          
      ),
    );
  }
}
