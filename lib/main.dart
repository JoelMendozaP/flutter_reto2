import 'package:flutter/material.dart';
import 'home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  final List<Widget> widgetsChildren = [
    Home(),
    Container(color: Colors.indigo,),
    Container(color: Colors.lime,),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: cuerpo(),
    );
  }

  Widget cuerpo() {
    return DefaultTabController(
      length: 3, 
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: Text("LUGARES",style: TextStyle(color: Colors.white, fontSize: 20)),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(icon: Icon(Icons.home, color: Colors.white, size: 20),),
              Tab(icon: Icon(Icons.directions_run, color: Colors.white, size: 20),),
              Tab(icon: Icon(Icons.accessibility_new, color: Colors.white, size: 20),),
            ],
          ),
        ),
        body: TabBarView(children: widgetsChildren)
        ),
      );
  }
}
